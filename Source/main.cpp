//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include "cmath"
#include "MidiMessage.h"


int main (int argc, const char* argv[])
{
    
    MidiMessage note;
 
    int value;
    std::cin >> value;
    note.setNoteNumber(value);
    
    std::cout << note.getNoteNumber() << "\n" << note.getMidiNoteInHertz();
    
    return 0;
}

