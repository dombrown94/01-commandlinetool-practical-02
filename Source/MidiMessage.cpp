//
//  MidiMessage.cpp
//  CommandLineTool
//
//  Created by Dominic Brown on 10/2/14.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//

#include "MidiMessage.h"
#include "cmath"

MidiMessage::MidiMessage()
{
    noteNumber = 60;
    velocity = 127;
    channel = 1;
}
MidiMessage::~MidiMessage()
{
}
void MidiMessage::setNoteNumber(int newNoteNumber)
{
    noteNumber = newNoteNumber;
}
void MidiMessage::setVelocity(int newVelocity)
{
    velocity = newVelocity;
}
void MidiMessage::setChannel(int newChannel)
{
    channel = newChannel;
}
int MidiMessage::getNoteNumber() const
{
    return noteNumber;
}
int MidiMessage::getVelocity() const
{
    return velocity;
}
int MidiMessage::getChannel() const
{
    return channel;
}
float MidiMessage::getMidiNoteInHertz() const
{
    return 440 * pow(2, (noteNumber-69)/12.0);
}
float MidiMessage::getFloatVelocity() const
{
    return velocity/127.0;
}