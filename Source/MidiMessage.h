//
//  MidiMessage.h
//  CommandLineTool
//
//  Created by Dominic Brown on 10/2/14.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//

#ifndef H_MIDIMESSAGE
#define H_MIDIMESSAGE

#include <iostream>

class MidiMessage
{
public:
    /** Constructor */
    MidiMessage();
    
    /** Destructor */
    ~MidiMessage();
    
    /** sets the MIDI note number of the message */
    void setNoteNumber(int newNoteNumber);
    
    /** sets the velocity value */
    void setVelocity(int newVelocity);
    
    /** sets the channel value */
    void setChannel(int newChannel);
    
    /** returns the note number of the message */
    int getNoteNumber() const;
    
    /** returns the velocity value */
    int getVelocity() const;
    
    /** returns channel data */
    int getChannel() const;
    
    /** returns the note number as a freq */
    float getMidiNoteInHertz() const;
    
    /** returns velocity as a float */
    float getFloatVelocity() const;
    
private:
    int noteNumber;
    int velocity;
    int channel;
};

#endif // H_MIDIMESSAGE